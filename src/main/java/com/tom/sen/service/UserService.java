package com.tom.sen.service;

import cn.hutool.core.util.RandomUtil;
import com.tom.sen.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserService {

    private List<User> userList=new ArrayList<>();

    @PostConstruct
    public void init(){

        for (int i = 0; i < 10; i++) {
            User user=new User();
            user.setAddress("北京市昌河区桐柏镇花园街道还原小区");
            user.setPhone(RandomUtil.randomNumbers(11));
            user.setUserName(RandomUtil.randomString(3));
            user.setPassWord(RandomUtil.randomString(6));
            userList.add(user);

        }

    }

    public User getUserById(Integer id) {
        User user = userList.get(id);
        log.info("user:{}",user);
        return user;
    }
}
