package com.tom.sen.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tom.sen.base.SensitiveInfoSerialize;
import com.tom.sen.base.SensitiveRole;
import com.tom.sen.base.SensitiveType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * https://mp.weixin.qq.com/s/RujXqk0MWhmtQKpf_zaTnQ
 *Java注解实现数据脱敏-与主业务解耦且权限管控关联
 *
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside  //让此注解可以被Jackson扫描到
@JsonSerialize(using = SensitiveInfoSerialize.class)  //配置处理此注解的序列化处理类
public @interface SensitiveInfo {


    /**
     * 拥有此权限不加密
     * @return
     */
    public SensitiveRole role() default SensitiveRole.no_seer;


    /**
     * 脱敏  加密的类型
     * @return
     */
    public SensitiveType type() default SensitiveType.MOBILE_PHONE;
}


