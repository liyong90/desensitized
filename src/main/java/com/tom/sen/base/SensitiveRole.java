package com.tom.sen.base;

public enum SensitiveRole {

    /**
     * 中文名
     */
    name_seer,

    /**
     * 身份证号
     */
    id_card_seer,
    /**
     * 座机号
     */
    fixed_phone_seer,
    /**
     * 手机号
     */
    mobile_phone_seer,
    /**
     * 地址
     */
    address_seer,
    /**
     * 电子邮件
     */
    email_seer,
    /**
     * 银行卡
     */
    bank_card_seer,
    /**
     * 公司开户银行联号
     */
    cnaps_code_seer,
    /**
     * 账户
     */
    account_no_seer,

    /**
     * 查看所有的
     */
    seer,

    /**
     * 所有都不能
     */
    no_seer
}
