package com.tom.sen.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;




@ConfigurationProperties(prefix = "log.shield")
@Configuration
public class LogShieldProperties {

    /**
     * 开关
     */
    private boolean enable;

    /**
     * 需要脱敏的包集合 多个以 【,】 隔开
     */
    private String includePackages;


    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getIncludePackages() {
        return includePackages;
    }

    public void setIncludePackages(String includePackages) {
        this.includePackages = includePackages;
    }
}
