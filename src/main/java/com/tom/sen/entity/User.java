package com.tom.sen.entity;

import com.tom.sen.annotation.SensitiveInfo;
import com.tom.sen.base.SensitiveType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class User implements Serializable {
    private static final long serialVersionUID = -8723871831790704729L;

    @SensitiveInfo(type=SensitiveType.CHINESE_NAME)
    private String userName;

    private String passWord;

    @SensitiveInfo()
    private String phone;

    private String address;
}
