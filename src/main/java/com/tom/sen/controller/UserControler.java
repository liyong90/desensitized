package com.tom.sen.controller;

import com.tom.sen.base.R;
import com.tom.sen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserControler {

    @Autowired
    private UserService userService;



    @GetMapping("/{id}")
    public R getUser(@PathVariable("id") Integer id){
        return R.ok(userService.getUserById(id));
    }
}
