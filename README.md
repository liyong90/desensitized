# Desensitized

#### 介绍
一个注解搞定系统脱敏（接口脱敏，日志脱敏 ），不需要多配置

#### 软件架构
软件架构说明



#### 使用说明

1.  SensitiveInfo注解配置脱敏的数据上
2.  配置log.shield配置
3.  loback.xml添加conversionRule

#### 参与贡献

基于gitee 源码改编，侵权必删
 * https://gitee.com/guofb/log-shield-spring-boot-starter
 * https://gitee.com/JustryDeng/logback-defender


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
